﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeisanClas
{
   
        public class function
        {

            public void menu(int n)
            {
            if (n == 1)
            {
                Console.WriteLine("Меню");
                Console.WriteLine("1. Решить линейное уравнение");
                Console.WriteLine("2. Решить квадратное уравнение");
                Console.WriteLine("3. Решить кубическое уравнение");
                Console.WriteLine("4. Найти i-ый член арифметической прогрессии");
                Console.WriteLine("5. Найти сумму арифметической прогрессии");
                Console.WriteLine("6. Найти сумму геометрической прогрессии");
                Console.WriteLine("7. Найти площадь круга");
                Console.WriteLine("8. Найти площадь треугольника");
                Console.WriteLine("9. Найти объём параллелепипеда");
                Console.WriteLine("10. Выход");
            }
            if (n == 2)
                {
                    Console.WriteLine("Введите коэффициенты при 'x' ");
                }
            }

            public int koef()
            {
                int koef = 0;
                koef = Convert.ToInt32(Console.ReadLine());
                return koef;
            }

            public List<double> Kub_Equal(int a, int b, int c, int d)
            {
                double y;
                int x = 0;
                List<double> z = new List<double>();
                for (x = -1000; x < 1000; x++)
                {
                    y = a * Math.Pow(x, 3) + b * Math.Pow(x, 2) + c * x + d;
                    if (y == 0)
                    {
                        z.Add(x);
                    }
                }
                return z;
            }

            public double koef_geo_prog(double S1, double S2, int N1, int N2)
            {
                double k = 0;
                for (double q = -1000; q <= 1000; q++)
                    if ((Math.Pow(q, N1) - 1) / (Math.Pow(q, N2) - 1) == S1 / S2)
                        k = q;
                return k;
            }

            public double first_member_geo_prog(double k, int N, double S)
            {
                double A1 = 0;
                A1 = S * (k - 1) / (Math.Pow(k, N) - 1);
                return A1;
            }

            public double koef_geo_prog_1(double S, double A1, int N)
            {
                double k = 0;
                for (double q = -1000; q <= 1000; q++)
                    if ((Math.Pow(q, N) - 1) / (q - 1) == S / A1)
                        k = q;
                return k;
            }

            public double sum_geo_prog(double k, double A1, int N)
            {
                double S3 = 0;
                S3 = A1 * (Math.Pow(k, N) - 1) / (k - 1);
                return S3;
            }

            public void print(List<double> x)
            {
                Console.Write("Корни уравнения: ");
                for (int i = 0; i < x.Count(); i++)
                {
                    Console.Write(x[i] + "; ");
                }
                Console.WriteLine();
            }
            public double area (double r)
        {
            double Area = 0;
            return Area = 3.14 * r * r;
        }                
        public double lin_ur(double a, double b)
        {
            double x = -1*b/a;                         
            return x;
        }

        public int cifr_i(int k)
        {
            Random rnd = new Random();                   
            int a = rnd.Next(1, k);
            return a;
        }

        public double cifr(int k)
        {
            Random rnd = new Random();                   
            double a = rnd.Next(1, k);
            return a;
        }
                                  

        public double _ai(double a1, double d, int i)
        {

            double ai = a1 + d * (i - 1);


            return ai;                                   
        }

        public double koren(double a, double b)
        {
            double x = 0;
            x = (-b / (2 * a));    
            return x;
        }

        public List<double> koren2(double a, double b, double D)
        {
            List<double> x = new List<double>();

            x.Add((-b - Math.Sqrt(D)) / (2 * a));
            x.Add((-b + Math.Sqrt(D)) / (2 * a));

            return x;
        }

        public double summa(double a1, double n, double an)
        {
            double S;
            S = (((a1 + an) * n) / 2);              
            return S;
        }

        public double triangle(double a, double h)
        {
            double S = 0.5 * a * h;
            return S;
        }

        public double objem(double a, double b, double c)
        {
            double V;
            V = a * b * c;
            return V;
            
        }
    }                      
}
