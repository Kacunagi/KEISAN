﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KeisanClas;

namespace Keisan2
{                               
    class Program
    {

        static void Main(string[] args)
        {                              
            List<double> x = new List<double>();
            function f = new function();



            int n = 11;
            while (n != 10)
            {
                Console.Clear();
                f.menu(1);
                Console.WriteLine("Выбирите пункт");
            
            {
                n = Convert.ToInt32(Console.ReadLine());
                switch (n)
                {
                    case 1:
                        Console.WriteLine("Линейное уравнение имеет вид ax+b=0 ");
                        Console.WriteLine("Введите a: ");
                        double a_koef = Convert.ToDouble(Console.ReadLine());
                        Console.WriteLine("Введите b: ");
                        double b_koef = Convert.ToDouble(Console.ReadLine());
                        Console.WriteLine("Ответ: x = " + f.lin_ur(a_koef,b_koef));                            
                        break;
                    case 2: 
                            {
                                double a, b, c, D;

                                Console.WriteLine("Введите значение элемента a ");

                                a = Convert.ToDouble(Console.ReadLine());

                                Console.WriteLine("Введите значение элемента b ");

                                b = Convert.ToDouble(Console.ReadLine());

                                Console.WriteLine("Введите значение элемента c ");

                                c = Convert.ToDouble(Console.ReadLine());

                                D = b * b - 4 * a * c;

                                if (D > 0)
                                {                                     
                                    x = f.koren2(a, b, D);
                                    Console.WriteLine("х1 = " + x[0] + " x2 = " + x[1]);
                                    x.Clear();
                                }
                                else if(D==0)
                                {
                                    Console.WriteLine("x = " +f.koren(a, b));
                                }
                                else
                                {
                                    Console.WriteLine("Корней нет!");
                                }
                            }
                            break;   
                    case 3:
                            {
                                f.menu(2);
                                int a, b, c, e;
                                a = f.koef();
                                b = f.koef();
                                c = f.koef();
                                e = f.koef();
                                x = f.Kub_Equal(a, b, c, e);
                                f.print(x);
                                break;
                            }
                    case 4:
                            {
                                Console.WriteLine("1. Найти определённый член арифметической прогрессии");
                                Console.WriteLine("2. Найти случайный член арифметической прогрессии");
                                int var = Console.Read();

                                switch (var)
                                {
                                    case 1:
                                        {
                                            Console.WriteLine("Введите i (номер члена арифметической прогрессии): ");
                                            int i = Convert.ToInt32(Console.ReadLine());
                                            if (i <= 0)
                                            {
                                                Console.WriteLine("Введено некорректное значение, попробуйте ещё раз: ");
                                                i = Convert.ToInt32(Console.ReadLine());
                                            }
                                            else
                                            {

                                                Console.WriteLine("Введите d (разность арифметической прогрессии): ");
                                                double p = Convert.ToDouble(Console.ReadLine());
                                                Console.WriteLine("Введите a1 (значение первого элемента арифметической прогрессии): ");
                                                double a_1;
                                                a_1 = Convert.ToDouble(Console.ReadLine());
                                                Console.WriteLine("Ответ: ai = " + f._ai(a_1, p, i));
                                                Console.ReadKey();
                                            }

                                            break;
                                        }
                                    case 2:
                                        {
                                            int i = f.cifr_i(90);
                                            double d_r = f.cifr(50);
                                            double a1_r = f.cifr(90);
                                            Console.WriteLine("номер члена арифметической прогрессии i = " + i);
                                            Console.WriteLine("разность арифметической прогрессии d = " + d_r);
                                            Console.WriteLine("значение первого элемента арифметической прогрессии a1 = " + a1_r);
                                            Console.WriteLine("Ответ: ai = " + f._ai(a1_r, d_r, i));
                                            break;
                                        }
                                    default:
                                        Console.WriteLine("Нет такого варианта");
                                        break;
                                }
                                break;
                            }
                    case 5:   
                            {
                                double a1, an, m;
                                Console.WriteLine("\nВычисление суммы арифметической прогрессии ");
                                Console.WriteLine("\nВведите значение первого элемента в прогрессии ");

                                a1 = Convert.ToInt32(Console.ReadLine());

                                Console.WriteLine("\nВведите значение последнего элемента в прогрессии ");

                                an = Convert.ToInt32(Console.ReadLine());                                

                                Console.WriteLine("\nВведите количество элементов в прогрессии ");

                                m = Convert.ToInt32(Console.ReadLine());

                                Console.WriteLine("\nСумма арифметической прогрессии = "+ f.summa(a1, m, an));                                   
                            }

                            break;          
                    case 6:
                        {
                            Console.WriteLine("Найти сумму N-членов геометрической прогрессии S, если известно:");
                            Console.WriteLine("1. Известен первый элемент(A1), количество элементов(N1), cумма N1-членов геометрической прогрессии(S1).");
                            Console.WriteLine("2. Известны N1 и N2, S1 и S2.");
                            Console.WriteLine("3. Известен коэффициент(k), первый член геометрической прогрессии(A1) и количество членов геометрической прогрессии(N).");
                            int z = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("Введите количество элементов складываемых элементов геометрической прогрессии");
                            int N = Convert.ToInt32(Console.ReadLine());
                            if (z == 1)
                            {
                                Console.WriteLine("Введите первый элемент(A1), количество элементов(N1), cумма N1-членов геометрической прогрессии(S1)");
                                double A1 = Convert.ToDouble(Console.ReadLine());
                                int N1 = Convert.ToInt32(Console.ReadLine());
                                double S = Convert.ToDouble(Console.ReadLine());
                                double k = f.koef_geo_prog_1(S, A1, N1);
                                Console.WriteLine("Сумма N-членов геометрической прогрессии равно : " + f.sum_geo_prog(k, A1, N));
                            }
                            if (z == 2)
                            {
                                Console.WriteLine("Введите N1 и N2, S1 и S2.");
                                int N1 = Convert.ToInt32(Console.ReadLine());
                                double S1 = Convert.ToDouble(Console.ReadLine());
                                int N2 = Convert.ToInt32(Console.ReadLine());
                                double S2 = Convert.ToDouble(Console.ReadLine());
                                double k = f.koef_geo_prog(S1, S2, N1, N2);
                                double A1 = f.first_member_geo_prog(k, N1, S1);
                                
                                Console.WriteLine("Сумма N-членов геометрической прогрессии равно : " + f.sum_geo_prog(k, A1, N));

                            }
                            if (z == 3)
                            {
                                Console.WriteLine("Введите коэффициент(k), первый член геометрической прогрессии(A1) и количество членов геометрической прогрессии(N).");
                                double A1 = Convert.ToDouble(Console.ReadLine());
                                double k = Convert.ToDouble(Console.ReadLine());
                                Console.WriteLine("Сумма N-членов геометрической прогрессии равно : " + f.sum_geo_prog(k, A1, N));
                            }
                        }
                        break;
                    case 7:
                        Console.WriteLine("1. Ввести радиус");
                        Console.WriteLine("2. Ввести диаметр");
                        int l = Convert.ToInt32(Console.ReadLine());
                        double r = Convert.ToDouble(Console.ReadLine());
                        switch (l)
                        {
                            case 1:                                         
                                Console.WriteLine("Площадь равна: " + f.area(r));
                                break;
                            case 2:                                                                       
                                Console.WriteLine("Площадь равна: " + f.area(r/2));
                                break;
                            default:
                                Console.WriteLine("Такого пункта нет. Выбирите другой вариант.");
                                break;
                        }
                        break;
                    case 8:
                            {
                                Console.WriteLine("\nВведите значение длины ");    
                                double a = Convert.ToInt32(Console.ReadLine());    
                                Console.WriteLine("\nВведите значение ширины ");
                                double b = Convert.ToInt32(Console.ReadLine());    
                                Console.WriteLine("\nВведите значение высоты ");
                                double c = Convert.ToInt32(Console.ReadLine());    
                                Console.WriteLine("Объём параллелепипеда равен " +f.objem(a, b, c));
                            }
                            break;
                        case 9:
                            {
                                Console.WriteLine("Введите длину стороны треугольника");
                                double a = Convert.ToDouble(Console.ReadLine());
                                Console.WriteLine("Введите длину высоты, проведённой к этой стороне треугольника");
                                double h = Convert.ToDouble(Console.ReadLine());
                                Console.WriteLine("Площадь треугольника равна " + f.triangle(a, h));
                            }
                            break;
                        case 10:
                            break;
                    default:
                        Console.WriteLine("Такого пункта нет. Выбирите другой вариант.");
                        break;
                }
                    Console.ReadKey();
                }                     
            }
        }
    }
}
