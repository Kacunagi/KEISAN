﻿using KeisanClas;
using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KeisanClas.Tests
{
    [TestClass]
    public class KeisanClasTest
    {
        [TestMethod]

        public void Kub_Equal_1andminus5_8andminus4return1and2()
        {
            //arrange
            int a = 1;                 
            int b = -5;
            int c = 8;
            int d = -4;
            List<double> expected = new List<double>();
            expected.Add(1);
            expected.Add(2);
            //act
            function f = new function();
            List<double> actual = f.Kub_Equal(a, b, c, d);
            //assert

            Assert.AreEqual(expected[0], actual[0],0.1); 
            Assert.AreEqual(expected[1], actual[1], 0.1);

        }
        [TestMethod]
        public void koef_geo_prog_30_62_4_5_return2()
        {
            //arrange
            double S1 = 30;
            double S2 = 62;
            int N1 = 4;
            int N2 = 5;
            double expected = 2;
            //act
            function f = new function();
            double actual = f.koef_geo_prog(S1, S2, N1, N2);
            //assert
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void first_member_geo_prog_2_6_63_return_1()
        {
            //arrange
            double k = 2;
            int N = 6;
            double S = 63;
            double expected = 1;
            //act
            function f = new function();
            double actual = f.first_member_geo_prog(k, N, S);
            //assert
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void koef_geo_prog_1_63_1_6_return_2()
        {
            //arrange
            double S = 63;
            double A1 = 1;
            int N = 6;
            double expected = 2;
            //act
            function f = new function();
            double actual = f.koef_geo_prog_1(S, A1, N);
            //assert
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void sum_geo_prog_2_1_6_return_63()
        {
            //arrange
            double k = 2;
            double A1 = 1;
            int N = 6;
            double expected = 63;
            //act
            function f = new function();
            double actual = f.sum_geo_prog(k, A1, N);
            //assert
            Assert.AreEqual(expected, actual);
        }        
        [TestMethod]
        public void area_2_return_12point56()
        {
            //arrange
            double r = 2;
            double expected = 12.56;
            //act
            function f = new function();
            double actual = f.area(r);
            //assert
            Assert.AreEqual(expected, actual);
        }
        [TestMethod()]
        public void lin_urTest()
        {
            //arrange
            double a = 7;
            double b = 28;
            double expected = -4;
            //act
            function f = new function();
            double actual = f.lin_ur(a, b);
            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]

        public void _ai_Test()
        {
            //arrange
            double a1 = 5;
            double d = 3;
            int i = 7;
            double expected = 23;
            //act
            function f = new function();
            double actual = f._ai(a1, d, i);
            //assert
            Assert.AreEqual(expected, actual);
        }
        

        [TestMethod]                                    
        public void koren_test()
        {
            //arrange
            double a = 5;
            double b = 10;
            double expected = -1;
            //act
            function f = new function();
            double actual = f.koren(a, b);
            //assert
            Assert.AreEqual(expected, actual, 0.1);
        }

        [TestMethod]      
        public void koren2_test()
        {
            //arrange
            double a = 2;
            double b = 10;
            double D = 36;
            List<double> expected = new List<double>();        
            expected.Add(-1);
            expected.Add(-4);                                  
            //act
            function f = new function();
            List<double> actual = f.koren2(a, b, D);           
            //assert
            Assert.AreEqual(expected[0], actual[1], 0.1);
            Assert.AreEqual(expected[1], actual[0], 0.1);
        }

        [TestMethod]                                                
        public void summa_test()
        {
            //arrange
            double a1 = 2;
            double an = 18;
            double n = 5;
            double expected = 50;
            //act
            function f = new function();
            double actual = f.summa(a1, n, an);
            //assert
            Assert.AreEqual(expected, actual, 0.1);
        }

        [TestMethod]

        public void area_test()
        {
            //arrange
            double r = 1;
            double expected = 3.14;
            //act
            function f = new function();
            double actual = f.area(r);
            //assert
            Assert.AreEqual(expected, actual, 0.1);
        }

        [TestMethod]

        public void triangle_test()
        {
            //arrange
            double a = 2;
            double h = 2;
            double expected = 2;
            //act
            function f = new function();
            double actual = f.triangle(a, h);
            //assert
            Assert.AreEqual(expected, actual, 0.1);
        }

        [TestMethod]

        public void objem_test()
        {
            //arrange
            double a = 2;
            double b = 2;
            double c = 2;
            double expected = 8;
            //act
            function f = new function();
            double actual = f.objem(a, b, c);
            //assert
            Assert.AreEqual(expected, actual, 0.1);
        }
        //Конец тестов
    }
}
